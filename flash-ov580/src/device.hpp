/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#include <string>

struct Device {
	public:
	Device(const std::string& deviceName, bool verbose = false);
	~Device();

	//! @brief Read Device firmware and dump it.
	void readFirmware(const std::string& fwFileName);
	//! @brief Compare Device firmware with file.
	bool checkFirmware(const std::string& fwFileName);
	//! @brief Write a new firmware then check it.
	void writeFirmware(const std::string& fwFileName);
	//! @brief Get firmware version.
	void getFirmwareVersion();

	protected:
	bool _verbose;
	int _device;
	void SPIWeird();
	void SPIReadPage(std::uint32_t isp_address,
	                 std::uint8_t* buffer,
	                 std::uint16_t length);
	void SPIWritePage(std::uint32_t isp_address,
	                  std::uint8_t* buffer,
	                  std::uint16_t length);
	void SPIErasePage(std::uint32_t isp_address);
	void SPIErase();
};
