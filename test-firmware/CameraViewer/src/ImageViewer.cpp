//! @file
#include "ImageViewer.hpp"

#include <QLabel>
#include <QScrollBar>
#include <QWheelEvent>

ImageViewer::ImageViewer(QWidget* parent)
  : QScrollArea(parent)
  , _image()
  , _scaleFactor(1.0f)
  , _imageLabel(nullptr) {
	setupGUI();
}

QSize
ImageViewer::sizeHint() const {
	return _image.size();
}

QSize
ImageViewer::minimumSizeHint() const {
	return QSize(160, 120);
}

void
ImageViewer::onImage(QImage img) {
	_image      = img;
	QPixmap pix = QPixmap::fromImage(_image).scaledToWidth(_image.width() * _scaleFactor,
	                                                       Qt::FastTransformation);
	_imageLabel->setPixmap(pix);
	_imageLabel->adjustSize();
	updateGeometry();
}

void
ImageViewer::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		_mouseLeft = true;
		_mouseX    = event->x();
		_mouseY    = event->y();
	}
}

void
ImageViewer::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		_mouseLeft = false;
	}
}

void
ImageViewer::mouseMoveEvent(QMouseEvent* event) {
	if (_mouseLeft) {
		int movX = event->x() - _mouseX;
		int movY = event->y() - _mouseY;
		_mouseX  = event->x();
		_mouseY  = event->y();

		horizontalScrollBar()->setValue(horizontalScrollBar()->value() - movX);
		verticalScrollBar()->setValue(verticalScrollBar()->value() - movY);
	}
}

void
ImageViewer::wheelEvent(QWheelEvent* event) {
	if (event->delta() > 0) {
		zoomIn();
	} else if (event->delta() < 0) {
		zoomOut();
	}
}

void
ImageViewer::keyPressEvent(QKeyEvent* event) {
	if (event->key() == Qt::Key_Space) {
		_scaleFactor = 1.0f;
	}
}

void
ImageViewer::zoomIn() {
	_scaleFactor = _scaleFactor * 1.25f > 16.0f ? 16.0f : _scaleFactor * 1.25f;
}

void
ImageViewer::zoomOut() {
	_scaleFactor = (_scaleFactor / 1.25f) < 0.25f ? 0.25f : _scaleFactor / 1.25f;
}

void
ImageViewer::setupGUI() {
	setObjectName(QString::fromUtf8("ImageViewer"));
	setAlignment(Qt::AlignCenter);
	setBackgroundRole(QPalette::Dark);

	_imageLabel = new QLabel();
	_imageLabel->setBackgroundRole(QPalette::Base);

	setWidget(_imageLabel);

	// Default Image
	{
		_image             = QImage(320, 240, QImage::Format_RGB32);
		std::uint32_t* ptr = reinterpret_cast<std::uint32_t*>(_image.bits());
		for (int it = 0; it < _image.width() * _image.height(); ++it) {
			*ptr++ = 0xff000000; // + (it & 0xffffff);
		}
		onImage(_image);
	}
}
