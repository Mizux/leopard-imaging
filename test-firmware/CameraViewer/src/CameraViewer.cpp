//! @file
#include "CameraViewer.hpp"

#include "ImageViewer.hpp"
#include <QDebug>
#include <QGroupBox>
#include <QMessageBox>
#include <QVBoxLayout>
#include <chrono>
#include <iostream>
#include <thread>

//! @brief alias to use STL steady clock.
using Clock = std::chrono::steady_clock;

//! @brief Clamp pixel value in the range [0,255]
//! @param[in] v Pixel value to clamp.
//! @return The value clamped in the range [0,255].
static inline std::uint8_t
clamp(int v) {
	return std::uint8_t(std::uint32_t(v) <= 255 ? v : v > 0 ? 255 : 0);
}

void
Worker::run() {
	// Loop
	while (true == _widget->_isStarted) {
		Clock::time_point startTime = Clock::now();
		auto img                    = getImage();
		Clock::time_point endTime   = Clock::now();
		Q_EMIT(image(img));

		auto latency =
		  std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
		auto period = 1000 / _widget->_camera.format().frameRate;
		if (latency.count() < period)
			std::this_thread::sleep_for(std::chrono::milliseconds(period - latency.count()));
	} // End While
}

QImage
Worker::getImage() {
	QImage image;
	try {
		// 1) Get Image
		std::unique_ptr<Camera::Image> img = _widget->_camera.getImage();
		image = QImage(img->width, img->height, QImage::Format_RGB32);

		const auto& imageData = img->buffer;
		const int width       = img->width;
		const int height      = img->height;

		if (imageData == nullptr) {
			std::cerr << "Could not retrieve current image." << std::endl;
		} else {
			std::cerr << "width: " << width << " height: " << height << std::endl;
			// 2) Convert to QImage
			unsigned char red, green, blue;
			std::uint32_t* ptr = reinterpret_cast<std::uint32_t*>(image.bits());
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; x += 2) {
					int C  = imageData[(y * width + x) * 2 + 0] - 16;
					int D  = imageData[(y * width + x) * 2 + 1] - 128;
					int C2 = imageData[(y * width + x) * 2 + 2] - 16;
					int E  = imageData[(y * width + x) * 2 + 3] - 128;

					int valC1 = 298 * C;
					int valC2 = 298 * C2;
					int valB  = 517 * D;
					int valG  = -100 * D - 208 * E;
					int valR  = 409 * E;

					red    = clamp((valC1 + valR) >> 8);
					green  = clamp((valC1 + valG) >> 8);
					blue   = clamp((valC1 + valB) >> 8);
					*ptr++ = 0xff000000 + (red << 16) + (green << 8) + blue;
					// qImage.setPixel(x, y, (red << 16) + (green << 8) + blue);

					red    = clamp((valC2 + valR) >> 8);
					green  = clamp((valC2 + valG) >> 8);
					blue   = clamp((valC2 + valB) >> 8);
					*ptr++ = 0xff000000 + (red << 16) + (green << 8) + blue;
					// qImage.setPixel(x + 1, y, (red << 16) + (green << 8) + blue);
				}
			}
		}
	} // End Try
	catch (const std::exception& e) {
		std::cerr << "Camera::getImage() exception: " << e.what() << std::endl;
	}
	return image;
}

CameraViewer::CameraViewer(Camera& camera, QWidget* parent)
  : QWidget(parent)
  , _camera(camera)
  , _isStarted(false)
  , _worker(nullptr)
  , _imageViewer(nullptr) {
	setupGUI();
}

void
CameraViewer::onStart() {
	if (!_isStarted) {
		_isStarted = true;
		_worker->start();
	}
}

void
CameraViewer::onStop() {
	if (_isStarted) {
		_isStarted = false;
		_worker->wait();
	}
}

void
CameraViewer::onError(QString txt) {
	QMessageBox msgBox;
	msgBox.setText(QString("Error occured: ") + txt);
	msgBox.exec();

	onStop();
}

void
CameraViewer::setupGUI() {
	setObjectName(QString::fromUtf8("CameraViewer"));
	setLayout(new QVBoxLayout(this));
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

	_imageViewer = new ImageViewer();
	layout()->addWidget(_imageViewer);

	// Build Worker
	_worker = new Worker(this);
	connect(_worker, &Worker::image, this->_imageViewer, &ImageViewer::onImage);
	connect(_worker, &Worker::error, this, &CameraViewer::onError);
}
