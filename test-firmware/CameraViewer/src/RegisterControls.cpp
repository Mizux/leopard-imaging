//! @file
#include "RegisterControls.hpp"

#include "RegisterBox.hpp"
#include <CameraLIOV5640.hpp>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>

RegisterControls::RegisterControls(Camera& camera, QWidget* parent)
  : QWidget(parent)
  , _camera(camera) {
	setupGUI();
}

void
RegisterControls::onReadRegister() {
	try {
		std::uint16_t value =
		  dynamic_cast<CameraLIOV5640&>(_camera).readRegister(_regAddress->value());
		_regValue->setValue(value);
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::readRegister Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
RegisterControls::onWriteRegister() {
	try {
		dynamic_cast<CameraLIOV5640&>(_camera).writeRegister(_regAddress->value(),
		                                                     _regValue->value());
	} catch (const std::exception& e) {
		QMessageBox msgBox;
		msgBox.setText("Camera::writeRegister Error occured");
		msgBox.setInformativeText(e.what());
		msgBox.exec();
	}
}

void
RegisterControls::setupGUI() {
	setObjectName(QString::fromUtf8("CameraControls"));
	setLayout(new QVBoxLayout(this));

	_regAddress = new RegisterBox("Address");
	layout()->addWidget(_regAddress);

	_regValue = new RegisterBox("Value");
	layout()->addWidget(_regValue);

	QSpacerItem* spacer =
	  new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	layout()->addItem(spacer);

	// Button line
	{
		QHBoxLayout* buttonLayout = new QHBoxLayout();
		dynamic_cast<QVBoxLayout*>(layout())->addLayout(buttonLayout);

		_regReadButton = new QPushButton();
		_regReadButton->setText("Read");
		connect(
		  _regReadButton, &QPushButton::clicked, this, &RegisterControls::onReadRegister);
		buttonLayout->addWidget(_regReadButton);

		_regWriteButton = new QPushButton();
		_regWriteButton->setText("Write");
		connect(
		  _regWriteButton, &QPushButton::clicked, this, &RegisterControls::onWriteRegister);
		buttonLayout->addWidget(_regWriteButton);
	}
}
