# Introduction {#mainpage}
[TOC]

You can find here, programs to perform the test 
plan and read camera(s) register.  

# HowTo Build {#build}
## Dependencies {#build_deps}
Project use CMake >= 3.2, C++11, and v4l2 API (i.e. programs are linux only).

To install new cmake version on *old* Ubuntu (i.e. < 16.04) just retrieve a new version on ppa.
```sh
sudo add-apt-repository ppa:george-edison55/cmake-3.x -y
sudo apt-get update -qq
sudo apt-get install -qq build-essential cmake feh
```
otherwise juste use:
```sh
sudo apt-get install -qq build-essential cmake feh
```

## Using Pure CMake (Native) {#build_cmakenative}
On Desktop
```sh
mkdir build && cd build
cmake ..
make
```
## Using QiBuild (Native) {#build_qibuildnative}
You can use qibuild to build natively.

Build & Deploy:
```sh
qibuild configure --release
qibuild make
```
Then you can run program using:
```sh
./build-sys-linux-x86_64/sdk/bin/program ...
```

## Using QiBuild (Aldebaran cross-compilation for robot) {#build_qibuildcross}
I suppose you have already created your QiBuild Toolchain.

Build & Deploy:
```sh
qibuild configure -c robot --release
qibuild make -c robot
qibuild deploy -c robot --url nao@ip.of.the.robot:~/foo
```
On robot:
```sh
ssh nao@ip.of.the.robot
./foo/bin/program ...
```

### Retrieve images {#testing}
To retrieve images on Desktop:
```sh
scp nao@ip.of.the.robot:/home/nao/\*.ppm .
```
tips: clean your working dir from previous images to be sure to retrieve the new
ones

Display images:  
```sh
feh *.ppm
```

Convert Image to png:  
After installing **imagemagick** you can easily convert image to png using:
```sh
for i in *.ppm; do convert $i ${i%.ppm}.png; done
```

# LI-OV4689 {#liov4689}
To perform the test plan you can use [CameraLIOV4689_UT](CameraLIOV4689_UT/README.md) program.  

To read/write Register please use  [CameraLIOV4689-dbg](CameraLIOV4689-dbg/README.md)
program.  

# LI-OV5640 {#liov5640}
To perform the test plan you can use [CameraLIOV5640_UT](CameraLIOV5640_UT/README.md)
program.  

To read/write Register please use  [CameraLIOV5640-dbg](CameraLIOV5640-dbg/README.md)
program.  

# Misc {#misc}
Test plan images are generated using [plantuml](http://plantuml.com/).  
To regenerate it, you can use:
```sh
java -jar plantuml.jar -Tpng *.pu
```

