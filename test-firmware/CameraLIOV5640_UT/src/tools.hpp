//! @file
#pragma once
#include <gtest/gtest.h>

#include <Camera.hpp>
#include <chrono>

//! @brief Maximum number of Image to dump on error.
#define DEFECT_NB 5
//! @brief Number of Image to grabe when testing robustness.
#define IMG_LOOP 250

//! @brief Stores a Camera Format along with a Focus mode.
class FormatAF : public std::pair<Camera::Format, bool> {
	public:
	//! @brief Default constructor.
	//! @param[in] format The Format requested.
	//! @param[in] af Auto-focus is enabled.
	FormatAF(Camera::Format format, bool af);

	//! @brief Injects FormatAF state in the output stream.
	//! @param[in,out] os Output stream to fill.
	//! @param[in] formatAF Format object to log.
	//! @return the output stream.
	friend inline std::ostream& operator<<(std::ostream& os, const FormatAF& formatAF) {
		return os << "{" << formatAF.first
		          << ", focus: " << (formatAF.second == true ? "AF" : "Fixed") << "}";
	}
};

namespace std {
//! @brief Prints to string a FormatAF object.
//! @param[in] formatAF The FormatAF object to print.
//! @return a std::string containing the object state.
inline std::string
to_string(const FormatAF& formatAF) {
	std::stringstream ss;
	ss << formatAF;
	return ss.str();
}
} // namespace std

//! @brief Helper to Open, Init, SetFocus and SetFormat.
//! @param[in] cam The camera instance to use.
//! @param[in] format The format requested to setup.
void setupCameraLIOV5640(Camera& cam, const FormatAF& format);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance to use.
//! @param[in] format The format requested to setup.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV5640Stream(const std::string& testName,
                              Camera& cam,
                              const FormatAF& format,
                              bool enableDrop);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance to use.
//! @param[in] prevFormat The previous format used.
//! @param[in] currFormat The current format in used.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV5640Stream(const std::string& testName,
                              Camera& cam,
                              const FormatAF& prevFormat,
                              const FormatAF& currFormat,
                              bool enableDrop);

//! @brief Try to grabe few images (IMG_LOOP images)
//! @note Will write the first frame to testName_refImg.ppm as ppm (P6) if g_enableDump.
//! @note Will write the first DEFECT_NB Corrupted images if any.
//! @param[in] testName Name of the test used for writing images.
//! @param[in] cam The camera instance used.
//! @param[in] format The format requested to setup.
//! @param[in] enableDrop Add a sleep during the loop to drop some frame.
void testCameraLIOV5640PatternStream(const std::string& testName,
                                     Camera& cam,
                                     const FormatAF& format,
                                     bool enableDrop);
