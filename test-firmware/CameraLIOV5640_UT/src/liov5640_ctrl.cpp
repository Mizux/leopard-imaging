//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV5640.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern FormatAF g_format;

//! @test Checks CameraLIOV5640 Brightness info are correct.
TEST(Controls, CheckBrightnessInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Brightness Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_BRIGHTNESS));
	EXPECT_EQ(info.min, -255);
	EXPECT_EQ(info.max, 255);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 0);
}

//! @test Checks CameraLIOV5640 Brightness getter and setter are working.
TEST(Controls, TestBrightness) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Set Brightness (3)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_BRIGHTNESS, 3));
	SCOPED_TRACE(cam.device() + ": Check Brightness == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_BRIGHTNESS));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV5640Stream("TestBrighness3", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Check Brightness == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_BRIGHTNESS));

	SCOPED_TRACE(cam.device() + ": Set Brightness (5)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_BRIGHTNESS, 5));
	SCOPED_TRACE(cam.device() + ": Check Brightness == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_BRIGHTNESS));

	testCameraLIOV5640Stream("TestBrighness5", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Brightness == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_BRIGHTNESS));
}

//! @test Checks CameraLIOV5640 Contrast info are correct.
TEST(Controls, CheckContrastInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Contrast Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_CONTRAST));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 255);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 32);
}

//! @test Checks CameraLIOV5640 Contrast getter and setter are working.
TEST(Controls, TestContrast) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Set Contrast (7)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_CONTRAST, 7));
	SCOPED_TRACE(cam.device() + ": Check Contrast == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_CONTRAST));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV5640Stream("TestContrast7", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Check Contrast == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_CONTRAST));

	SCOPED_TRACE(cam.device() + ": Set Contrast (11)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_CONTRAST, 11));
	SCOPED_TRACE(cam.device() + ": Check Contrast == 11");
	EXPECT_EQ(11, cam.getParameter(V4L2_CID_CONTRAST));

	testCameraLIOV5640Stream("TestContrast11", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Contrast == 11");
	EXPECT_EQ(11, cam.getParameter(V4L2_CID_CONTRAST));
}

//! @test Checks CameraLIOV5640 Hue info are correct.
TEST(Controls, CheckHueInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Hue Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_HUE));
	EXPECT_EQ(info.min, -180);
	EXPECT_EQ(info.max, 180);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 0);
}

//! @test Checks CameraLIOV5640 Hue getter and setter are working.
TEST(Controls, TestHue) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Set Hue (30)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_HUE, 30));
	SCOPED_TRACE(cam.device() + ": Check Hue == 30");
	EXPECT_EQ(30, cam.getParameter(V4L2_CID_HUE));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV5640Stream("TestHue30", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Check Hue == 30");
	EXPECT_EQ(30, cam.getParameter(V4L2_CID_HUE));

	SCOPED_TRACE(cam.device() + ": Set Hue (90)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_HUE, 90));
	SCOPED_TRACE(cam.device() + ": Check Hue == 90");
	EXPECT_EQ(90, cam.getParameter(V4L2_CID_HUE));

	testCameraLIOV5640Stream("TestHue90", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Hue == 90");
	EXPECT_EQ(90, cam.getParameter(V4L2_CID_HUE));
}

//! @test Checks CameraLIOV5640 Saturation info are correct.
TEST(Controls, CheckSaturationInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Saturation Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_SATURATION));
	EXPECT_EQ(info.min, 0);
	EXPECT_EQ(info.max, 255);
	EXPECT_EQ(info.step, 1);
	EXPECT_EQ(info.def, 64);
}

//! @test Checks CameraLIOV5640 Saturation getter and setter are working.
TEST(Controls, TestSaturation) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Set Saturation (2)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SATURATION, 2));
	SCOPED_TRACE(cam.device() + ": Check Saturation == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_SATURATION));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV5640Stream("TestSaturation2", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Check Saturation == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_SATURATION));

	SCOPED_TRACE(cam.device() + ": Set Saturation (7)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SATURATION, 7));
	SCOPED_TRACE(cam.device() + ": Check Saturation == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_SATURATION));

	testCameraLIOV5640Stream("TestSaturation7", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Saturation == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_SATURATION));
}

//! @test Checks CameraLIOV5640 Sharpness info are correct.
TEST(Controls, CheckSharpnessInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Sharpness Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_SHARPNESS));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(9, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(4, info.def);
}

//! @test Checks CameraLIOV5640 Sharpness getter and setter are working.
TEST(Controls, TestSharpness) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Set Sharpness (2)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SHARPNESS, 2));
	SCOPED_TRACE(cam.device() + ": Check Sharpness == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_SHARPNESS));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV5640Stream("TestSharpness2", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Check Sharpness == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_SHARPNESS));

	SCOPED_TRACE(cam.device() + ": Set Sharpness (7)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SHARPNESS, 7));
	SCOPED_TRACE(cam.device() + ": Check Sharpness == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_SHARPNESS));

	testCameraLIOV5640Stream("TestSharpness7", cam, g_format, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Sharpness == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_SHARPNESS));
}

//! @test Checks CameraLIOV5640 Sharpness info are correct.
TEST(Controls, CheckFlipInfo) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV5640(cam, g_format);

	SCOPED_TRACE(cam.device() + ": Get Horizontal Flip Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info =
	                  cam.getExtUnitInfo(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(1, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(0, info.def);

	SCOPED_TRACE(cam.device() + ": Get Vertical Flip Info");
	ASSERT_NO_THROW(info =
	                  cam.getExtUnitInfo(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(1, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(0, info.def);
}

//! @test Checks CameraLIOV5640 Flip getter and setter are working.
TEST(Controls, TestFlip) {
	CameraLIOV5640 cam(g_device, g_preference, g_enableVerbose);
	SCOPED_TRACE(cam.device() + ": Set Flip (0)");
	setupCameraLIOV5640(cam, g_format);
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));

	SCOPED_TRACE(cam.device() + ": run UnFlip Stream");
	testCameraLIOV5640Stream("TestUnFlip", cam, g_format, false);
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (UVC)");
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(0, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 0 (Register)");
	EXPECT_EQ(0, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(6, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	// Set Flip to 1
	SCOPED_TRACE(cam.device() + ": Set Flip (1)");
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP, 1));
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP, 1));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));

	SCOPED_TRACE(cam.device() + ": run Flip Stream");
	testCameraLIOV5640Stream("TestFlip", cam, g_format, false);
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Check Flip == 1");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));

	// Restart should not change state
	SCOPED_TRACE(cam.device() + ": run Flip Stream");
	testCameraLIOV5640Stream("TestRestartFlip", cam, g_format, false);
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (UVC)");
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP));
	EXPECT_EQ(1, cam.getExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP));
	SCOPED_TRACE(cam.device() + ": Check Flip == 1 (Register)");
	EXPECT_EQ(6, cam.readRegister(0x3820) & 0x6); // Vertical
	EXPECT_EQ(0, cam.readRegister(0x3821) & 0x6); // Mirror

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	// Reset
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::VERTICAL_FLIP, 0));
	ASSERT_NO_THROW(cam.setExtUnit(CameraLIOV5640::ExtensionUnit::HORIZONTAL_FLIP, 0));
}
