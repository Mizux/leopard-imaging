//! @file
#include <CameraLIOV5640.hpp>

#include <cstring>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <thread>

extern "C" {
#include <linux/usb/video.h> // UVC Controls
#include <linux/uvcvideo.h>  // UVC ExtUnit Controls
#include <linux/videodev2.h> // V4L2 Controls
#include <sys/ioctl.h>       // ::ioctl
}
std::uint32_t
CameraLIOV5640::getVersion() const {
	return getExtUnit(ExtensionUnit::VERSION);
}

void
CameraLIOV5640::init() {
	CameraV4L2::init();

	setParameter(V4L2_CID_BRIGHTNESS, 0);
	setParameter(V4L2_CID_CONTRAST, 32);
	setParameter(V4L2_CID_SATURATION, 64);
	setParameter(V4L2_CID_HUE, 0);
	setParameter(V4L2_CID_SHARPNESS, 4);

	setExtUnit(ExtensionUnit::HORIZONTAL_FLIP, 0); // disable Horizontal Flip
	setExtUnit(ExtensionUnit::VERTICAL_FLIP, 0);   // disable Vertical Flip
	setExtUnit(CameraLIOV5640::TEST_PATTERN, 0x0); // disable Test Pattern

	setParameter(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_AUTO);
	setParameter(V4L2_CID_AUTO_WHITE_BALANCE, 1);
	setParameter(V4L2_CID_FOCUS_AUTO, 1); // enable auto focus
}

std::uint16_t
CameraLIOV5640::readRegister(std::uint16_t addr) const {
	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 3;
	xu_query.selector = 0x0e;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 5;

	std::uint8_t data[5];
	std::memset(data, 0, 5);
	data[0]       = 0; // Read
	data[1]       = addr >> 8;
	data[2]       = addr & 0xff;
	xu_query.data = data;
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}

	std::this_thread::sleep_for(std::chrono::seconds(1));

	xu_query.query = UVC_GET_CUR;
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_GET_CUR fails: " + std::strerror(errno));
	}

	return (std::uint16_t(data[3]) << 8) | std::uint16_t(data[4]);
}

void
CameraLIOV5640::writeRegister(std::uint16_t addr, std::uint16_t value) {
	struct uvc_xu_control_query xu_query;
	std::memset(&xu_query, 0, sizeof(xu_query));
	xu_query.unit     = 3;
	xu_query.selector = 0x0e;
	xu_query.query    = UVC_SET_CUR;
	xu_query.size     = 5;

	std::uint8_t data[5];
	std::memset(data, 0, 5);
	data[0]       = 1; // Write
	data[1]       = addr >> 8;
	data[2]       = addr & 0xff;
	data[3]       = value >> 8;
	data[4]       = value & 0xff;
	xu_query.data = data;
	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_query)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
	std::this_thread::sleep_for(std::chrono::seconds(1));
}

std::array<std::uint8_t, 17>
CameraLIOV5640::readExposureMetering() const {
	std::array<std::uint8_t, 17> data;
	struct uvc_xu_control_query xu_queryctrl;
	::memset(&xu_queryctrl, 0, sizeof(xu_queryctrl));
	xu_queryctrl.unit     = 3;
	xu_queryctrl.selector = 0x09;
	xu_queryctrl.query    = UVC_GET_CUR;
	xu_queryctrl.size     = 17;
	xu_queryctrl.data     = data.data();

	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_GET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
	return data;
}

void
CameraLIOV5640::writeExposureMetering(std::array<std::uint8_t, 17> values) {
	struct uvc_xu_control_query xu_queryctrl;
	::memset(&xu_queryctrl, 0, sizeof(xu_queryctrl));
	xu_queryctrl.unit     = 3;
	xu_queryctrl.selector = 0x09;
	xu_queryctrl.query    = UVC_SET_CUR;
	xu_queryctrl.size     = 17;
	xu_queryctrl.data     = values.data();

	if (-1 == ::ioctl(_fd, UVCIOC_CTRL_QUERY, &xu_queryctrl)) {
		std::cerr << "(ERROR) " << _device
		          << ": UVC_SET_CUR fails: " << std::strerror(errno) << std::endl;
		throw std::runtime_error(_device + ": UVC_SET_CUR fails: " + std::strerror(errno));
	}
	std::this_thread::sleep_for(std::chrono::seconds(1));
}
