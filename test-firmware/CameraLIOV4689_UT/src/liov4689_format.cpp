//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV4689.hpp>
#include <chrono>
#include <thread>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;
extern bool g_enableDump;

//! @brief Stores a list of available Format.
using FormatList = std::vector<Camera::Format>;
//! @brief Stores the list of available Format for CameraLIOV4689.
const FormatList g_FormatAFList = {{{1344, 376}, 10},
                                   {{1344, 376}, 15},
                                   {{1344, 376}, 30},
                                   {{2560, 720}, 10},
                                   {{2560, 720}, 15},
                                   {{2560, 720}, 30}};

class SingleFormatTest : public ::testing::TestWithParam<Camera::Format> {};

//! @test Checks CameraLIOV4689 latency to get the first Image.
TEST_P(SingleFormatTest, StartLatency) {
	SCOPED_TRACE("Test Start Latency for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());
	// Run one time the camera to avoid the latency involved by the switch
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Start Stream and Get the First Frame");
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	ASSERT_NO_THROW(cam.start());
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "[ INFO     ] StartLatency, " << cam.device() << ", "
	          << std::to_string(GetParam()) << ", " << duration.count() << std::endl;
	EXPECT_LE(duration, std::chrono::milliseconds(1500))
	  << "[ ERROR    ] StartTooLong, " << cam.device() << ", "
	  << std::to_string(GetParam()) << ", " << duration.count();
}

//! @test Checks CameraLIOV4689 Image corruption using internal test pattern.
TEST_P(SingleFormatTest, CheckTestPattern) {
	SCOPED_TRACE("Test Image Test Pattern for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV4689PatternStream(
	  "FormatCheckTestPattern_" + std::to_string(GetParam()), cam, GetParam(), false);
}

//! @test Checks CameraLIOV4689 Image corruption using internal test pattern while
//! Images are drop.
TEST_P(SingleFormatTest, CheckTestPatternWithDrop) {
	SCOPED_TRACE("Test Image Test Pattern with Drop for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV4689PatternStream(
	  "FormatCheckTestPatternWithDrop_" + std::to_string(GetParam()),
	  cam,
	  GetParam(),
	  true);
}

//! @test Checks CameraLIOV4689 streaming stability.
TEST_P(SingleFormatTest, CheckImage) {
	SCOPED_TRACE("Test Image for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV4689Stream(
	  "FormatCheckImage_" + std::to_string(GetParam()), cam, GetParam(), false);
}

//! @test Checks CameraLIOV4689 streaming stability while Images are drop.
TEST_P(SingleFormatTest, CheckImageWithDrop) {
	SCOPED_TRACE("Test Image for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start & Test Stream");
	testCameraLIOV4689Stream(
	  "FormatCheckImageWithDrop_" + std::to_string(GetParam()), cam, GetParam(), true);
}

//! @test Checks CameraLIOV4689 periods consistency between two consecutive Images.
TEST_P(SingleFormatTest, Regularity) {
	SCOPED_TRACE("Test Period for: " + std::to_string(GetParam()));
	std::int32_t expectedDelta = 1000 / GetParam().frameRate;
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam());

	SCOPED_TRACE(cam.device() + ": Start Stream");
	ASSERT_NO_THROW(cam.start());
	SCOPED_TRACE(cam.device() + ": Flush...");
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	ASSERT_TRUE(nullptr != img);
	std::uint32_t previousSeq = img->seq;
	std::int32_t previousTime = img->seconds * 1000 + img->milliseconds;

	SCOPED_TRACE(cam.device() + ": Get " + std::to_string(IMG_LOOP) + " Frames");
	for (std::size_t i = 1; i <= IMG_LOOP; ++i) {
		SCOPED_TRACE(cam.device() + ": loop: " + std::to_string(i));
		ASSERT_NO_THROW(img = cam.getImage())
		  << "[ ERROR    ] NoImage, " << cam.device() << ", " << std::to_string(GetParam())
		  << ", " << std::to_string(i);
		// Mean we didn't DQBUF enough quickly.
		EXPECT_EQ(img->seq, previousSeq + 1);

		std::int32_t currentTime  = img->seconds * 1000 + img->milliseconds;
		std::int32_t currentDelta = currentTime - previousTime;
		SCOPED_TRACE(cam.device() + ": Frames " + std::to_string(previousSeq) + ":" +
		             std::to_string(previousTime) + "ms, " + std::to_string(img->seq) +
		             ": " + std::to_string(currentTime) + "ms, Expected time: " +
		             std::to_string(previousTime + expectedDelta) + "ms");
		SCOPED_TRACE(cam.device() + ": Current Delta: " + std::to_string(currentDelta) +
		             "ms, Expected Delta: " + std::to_string(expectedDelta) + "ms");
		std::cout << "[ INFO     ] Delta, " << cam.device() << ", "
		          << std::to_string(GetParam()) << ", " << std::to_string(currentDelta)
		          << std::endl;

		// 5ms jitter max between two consecutive image according to format (i.e. fps).
		EXPECT_TRUE(std::abs(currentDelta - expectedDelta) <= 5)
		  << ((img->seq == previousSeq + 1) ? "[ ERROR    ] FrameDrop, " :
		                                      "[ ERROR    ] MissFrame, ")
		  << cam.device() << ", " << std::to_string(GetParam()) << ", "
		  << std::to_string(img->seq);
		previousSeq  = img->seq;
		previousTime = currentTime;
	}
	if (g_enableDump) {
		std::stringstream ss;
		ss << "FormatRegularity_" << std::to_string(GetParam()) << ".ppm";
		SCOPED_TRACE(cam.device() + ": Write image " + ss.str());
		EXPECT_NO_THROW(cam.writeImage2PPM(*img, ss.str()));
	}

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());
	SCOPED_TRACE(cam.device() + ": Close Device");
	ASSERT_NO_THROW(cam.closeDevice());
}

//! @brief Instantiate CameraLIOV4689 format tests.
INSTANTIATE_TEST_CASE_P(Format, SingleFormatTest, ::testing::ValuesIn(g_FormatAFList));

//! @brief Stores a pair of Format to check format change.
using FormatToFormat = std::pair<Camera::Format, Camera::Format>;
namespace std {
//! @brief Converts FormatToFormat value to std::string.
//! @param[in] in The value to convert.
//! @return the value converted.
inline std::string
to_string(const FormatToFormat& in) {
	return std::to_string(in.first) + "->" + std::to_string(in.second);
}
} // namespace std

//! @brief Generates exhaustive list of FormatToFormat possibilities.
//! @return a vector of all possible FormatToFormat.
std::vector<FormatToFormat>
generateFormatToFormatList() {
	std::vector<FormatToFormat> out;
	for (const auto& first : g_FormatAFList) {
		for (const auto& second : g_FormatAFList) {
			if (first == second) continue;
			out.push_back(std::make_pair(first, second));
		}
	}
	return out;
}

class SwitchFormatTest : public ::testing::TestWithParam<FormatToFormat> {};

//! @test Checks CameraLIOV4689 latency to get the first Image.
TEST_P(SwitchFormatTest, StartLatency) {
	SCOPED_TRACE("Test Start Latency for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam().first);
	// Run one time the camera with the previous format to compute the latency involved by
	// the switch
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.stop());

	// Run with the Second Format
	SCOPED_TRACE(cam.device() +
	             ": Set Second Format: " + std::to_string(GetParam().second));
	ASSERT_NO_THROW(cam.setFormat(GetParam().second));

	SCOPED_TRACE(cam.device() + ": Start Stream and Get the First Frame");
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	ASSERT_NO_THROW(cam.start());
	std::unique_ptr<Camera::Image> img;
	ASSERT_NO_THROW(img = cam.flushBuffers());
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	const Camera::Format& formatFirst  = GetParam().first;
	const Camera::Format& formatSecond = GetParam().second;
	std::cout << "[ INFO     ] StartLatency, " << cam.device() << ", "
	          << std::to_string(formatFirst) << ", " << std::to_string(formatSecond)
	          << ", " << duration.count() << std::endl;
	EXPECT_LE(duration, std::chrono::milliseconds(1500))
	  << "[ ERROR    ] StartTooLong, " << cam.device() << ", "
	  << std::to_string(formatFirst) << ", " << std::to_string(formatSecond) << ", "
	  << duration.count();
}

//! @test Checks CameraLIOV4689 streaming stability.
TEST_P(SwitchFormatTest, CheckImage) {
	SCOPED_TRACE("Test Formats switch for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": run with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": run with the Second Format");
	ASSERT_NO_THROW(cam.setFormat(GetParam().second));

	testCameraLIOV4689Stream("FormatSwitch_" + std::to_string(GetParam()),
	                         cam,
	                         GetParam().first,
	                         GetParam().second,
	                         false);
}

//! @test Checks CameraLIOV4689 streaming stability.
TEST_P(SwitchFormatTest, CheckImageWithDrop) {
	SCOPED_TRACE("Test Formats switch with drop for: " + std::to_string(GetParam()));
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, GetParam().first);

	SCOPED_TRACE(cam.device() + ": run with the First Format");
	ASSERT_NO_THROW(cam.start());
	ASSERT_NO_THROW(cam.flushBuffers());
	ASSERT_NO_THROW(cam.getImage());
	SCOPED_TRACE(cam.device() + ": stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": run with the Second Format");
	ASSERT_NO_THROW(cam.setFormat(GetParam().second));

	testCameraLIOV4689Stream("FormatSwitch_" + std::to_string(GetParam()),
	                         cam,
	                         GetParam().first,
	                         GetParam().second,
	                         true);
}

//! @brief Instantiate CameraLIOV4689 format to format tests.
INSTANTIATE_TEST_CASE_P(Format,
                        SwitchFormatTest,
                        ::testing::ValuesIn(generateFormatToFormatList()));
