//! @file
#include <gtest/gtest.h>

#include "tools.hpp"
#include <CameraLIOV4689.hpp>

extern std::string g_device;
extern Camera::Format g_preference;
extern bool g_enableVerbose;

//! @test Check CameraLIOV4689 Brightness info are correct.
TEST(Brightness, CheckBrightnessInfo) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Get Brightness Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_BRIGHTNESS));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(8, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(4, info.def);
}

//! @test Check CameraLIOV4689 Brightness getter and setter are working.
TEST(Brightness, TestBrightness) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Set Brightness (3)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_BRIGHTNESS, 3));
	SCOPED_TRACE(cam.device() + ": Check Brightness == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_BRIGHTNESS));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV4689Stream("TestBrighness3", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Check Brightness == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_BRIGHTNESS));

	SCOPED_TRACE(cam.device() + ": Set Brightness (7)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_BRIGHTNESS, 7));
	SCOPED_TRACE(cam.device() + ": Check Brightness == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_BRIGHTNESS));

	testCameraLIOV4689Stream("TestBrighness7", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Brightness == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_BRIGHTNESS));
}

//! @test Check CameraLIOV4689 Contrast info are correct.
TEST(Contrast, CheckContrastInfo) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Get Contrast Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_CONTRAST));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(8, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(4, info.def);
}

//! @test Check CameraLIOV4689 Contrast getter and setter are working.
TEST(Contrast, TestContrast) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Set Contrast (2)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_CONTRAST, 2));
	SCOPED_TRACE(cam.device() + ": Check Contrast == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_CONTRAST));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV4689Stream("TestContrast2", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Check Contrast == 2");
	EXPECT_EQ(2, cam.getParameter(V4L2_CID_CONTRAST));

	SCOPED_TRACE(cam.device() + ": Set Contrast (7)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_CONTRAST, 7));
	SCOPED_TRACE(cam.device() + ": Check Contrast == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_CONTRAST));

	testCameraLIOV4689Stream("TestContrast7", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Contrast == 7");
	EXPECT_EQ(7, cam.getParameter(V4L2_CID_CONTRAST));
}

//! @test Check CameraLIOV4689 Hue info are correct.
TEST(Hue, CheckHueInfo) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Get Hue Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_HUE));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(11, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(0, info.def);
}

//! @test Check CameraLIOV4689 Hue getter and setter are working.
TEST(Hue, TestHue) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Set Hue Auto to Manual");
	ASSERT_NO_THROW(cam.setParameter(V4L2_CID_HUE_AUTO, 0));

	SCOPED_TRACE(cam.device() + ": Set Hue (5)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_HUE, 5));
	SCOPED_TRACE(cam.device() + ": Check Hue == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_HUE));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV4689Stream("TestHue5", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Check Hue Auto == Manual");
	EXPECT_EQ(0, cam.getParameter(V4L2_CID_HUE_AUTO));
	SCOPED_TRACE(cam.device() + ": Check Hue == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_HUE));

	SCOPED_TRACE(cam.device() + ": Set Hue (3)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_HUE, 3));
	SCOPED_TRACE(cam.device() + ": Check Hue == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_HUE));

	testCameraLIOV4689Stream("TestHue3", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Stop Stream");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Hue == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_HUE));
}

//! @test Check CameraLIOV4689 Saturation info are correct.
TEST(Saturation, CheckSaturationInfo) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Get Saturation Info");
	Camera::ParameterInfo info;
	ASSERT_NO_THROW(info = cam.getParameterInfo(V4L2_CID_SATURATION));
	EXPECT_EQ(0, info.min);
	EXPECT_EQ(8, info.max);
	EXPECT_EQ(1, info.step);
	EXPECT_EQ(4, info.def);
}

//! @test Check CameraLIOV4689 Saturation getter and setter are working.
TEST(Saturation, TestSaturation) {
	CameraLIOV4689 cam(g_device, g_preference, g_enableVerbose);
	setupCameraLIOV4689(cam, g_preference);

	SCOPED_TRACE(cam.device() + ": Set Saturation (3)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SATURATION, 3));
	SCOPED_TRACE(cam.device() + ": Check Saturation == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_SATURATION));

	SCOPED_TRACE(cam.device() + ": Start Stream");
	testCameraLIOV4689Stream("TestSaturation3", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Check Saturation == 3");
	EXPECT_EQ(3, cam.getParameter(V4L2_CID_SATURATION));

	SCOPED_TRACE(cam.device() + ": Set Saturation (5)");
	EXPECT_NO_THROW(cam.setParameter(V4L2_CID_SATURATION, 5));
	SCOPED_TRACE(cam.device() + ": Check Saturation == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_SATURATION));

	testCameraLIOV4689Stream("TestSaturation5", cam, g_preference, false);

	SCOPED_TRACE(cam.device() + ": Stop");
	ASSERT_NO_THROW(cam.stop());

	SCOPED_TRACE(cam.device() + ": Check Saturation == 5");
	EXPECT_EQ(5, cam.getParameter(V4L2_CID_SATURATION));
}
