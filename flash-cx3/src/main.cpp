/*! @file
@author Corentin LE MOLGAT <clemolgat@softbankrobotics.com>
@copyright This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#include "FX3Device.hpp"
#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

inline void
getUsage(const std::string& argv0) {
	std::cout << "usage: " << std::endl
	          << argv0 << " [OPTIONS] -p bus:port.port -f file" << std::endl
	          << argv0 << " [OPTIONS] -d vendor:product -f file" << std::endl
	          << "-p, --path <bus:port.port>: Specify device by its path (e.g. -p 3:1.2, "
	             "for bus 3 and ports 1.2)"
	          << std::endl
	          << "-d, --device <vendor:product>: Specify device by its "
	             "vendorID:ProductID (e.g. -d 04b4:00f3)"
	          << std::endl
	          << "-f <file>: Flash the device with the firmware <file>" << std::endl
	          << "OPTIONS:" << std::endl
	          << "-h, --help: Show usage and help" << std::endl
	          << "-v, --verbose: enable verbose log" << std::endl;
}

inline std::string
getCmdOption(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	std::vector<std::string>::const_iterator itr =
	  std::find(args.begin(), args.end(), option);
	if (itr != args.end() && ++itr != args.end()) {
		return *itr;
	}
	return std::string();
}

inline bool
isCmdExists(int argc, char* argv[], const std::string& option) {
	std::vector<std::string> args(argv, argv + argc);
	return std::find(args.begin(), args.end(), option) != args.end();
}

bool
getVerbosity(int argc, char** argv) {
	if (isCmdExists(argc, argv, "-v") || isCmdExists(argc, argv, "--verbose")) {
		return true;
	}
	return false;
}

struct Path {
	std::uint8_t bus;
	std::vector<std::uint8_t> ports;
};

Path
getPath(int argc, char** argv) {
	std::string input;
	if (isCmdExists(argc, argv, "-p")) {
		input = getCmdOption(argc, argv, "-p");
	} else if (isCmdExists(argc, argv, "--path")) {
		input = getCmdOption(argc, argv, "--path");
	} else {
		throw std::runtime_error("No path specified !");
	}
	Path result;
	{
		std::istringstream ss(input);
		std::string token;
		// First Get Bus string
		std::getline(ss, token, ':');
		result.bus = std::stoi(token);
		// Second Get Ports string
		std::getline(ss, token, ':');
		ss.str(token);
		ss.seekg(0);
		while (std::getline(ss, token, '.')) {
			result.ports.push_back(std::stoi(token));
		}
	}
	return result;
}

struct Device {
	std::uint16_t vendorID;
	std::uint16_t productID;
};

Device
getDevice(int argc, char** argv) {
	std::string input;
	if (isCmdExists(argc, argv, "-d")) {
		input = getCmdOption(argc, argv, "-d");
	} else if (isCmdExists(argc, argv, "--device")) {
		input = getCmdOption(argc, argv, "--device");
	} else {
		throw std::runtime_error("No device specified !");
	}
	Device result;
	{
		std::stringstream ss;
		ss.str(input);
		std::string token;
		// First Get VendorID string
		std::getline(ss, token, ':');
		result.vendorID = std::stoi(token, 0, 16);
		// Second Get Ports string
		std::getline(ss, token, ':');
		result.productID = std::stoi(token, 0, 16);
	}
	return result;
}

int
main(int argc, char** argv) {
	if (argc < 2 || isCmdExists(argc, argv, "-h") || isCmdExists(argc, argv, "--help")) {
		getUsage(argv[0]);
		return 0;
	}

	// Firmware file
	if (!isCmdExists(argc, argv, "-f")) {
		std::cerr << "(ERROR) Please specify a firmware file !" << std::endl;
		getUsage(argv[0]);
		return 1;
	}
	std::string filename = getCmdOption(argc, argv, "-f");
	std::cout << "(INFO) Firmware: " << filename << std::endl;

	// Device by path
	std::unique_ptr<FX3Device> fx3Device;
	if (isCmdExists(argc, argv, "-p") || isCmdExists(argc, argv, "--path")) {
		auto path = getPath(argc, argv);
		fx3Device.reset(new FX3Device(path.bus, path.ports, getVerbosity(argc, argv)));
	} else if (isCmdExists(argc, argv, "-d") || isCmdExists(argc, argv, "--device")) {
		auto device = getDevice(argc, argv);
		fx3Device.reset(
		  new FX3Device(device.vendorID, device.productID, getVerbosity(argc, argv)));
	} else {
		std::cerr << "(ERROR) Please specify a device !" << std::endl;
		getUsage(argv[0]);
		return 2;
	}

	std::cout << "(INFO) Found CX3 device: " << *fx3Device << std::endl;
	fx3Device->flash(filename);
	return 0;
}
